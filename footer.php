
                </div>

        </div><!-- contextWrap -->

        <script src="<?php echo $host;?>/library/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo $host;?>/library/js/jquery-ui.min.js"></script>
        <script src="<?php echo $host;?>/library/semantic/semantic.min.js"></script>

        <script src="<?php echo $host;?>/library/plugins/sweetalert2/sweetalert2.min.js"></script>
        <script src="<?php echo $host;?>/library/plugins/quill/quill.js"></script>
        <script src="<?php echo $host;?>/library/plugins/hold_on/src/js/HoldOn.js"></script>
        <script src="<?php echo $host;?>/library/plugins/mask/dist/jquery.mask.js"></script>
        <script src="<?php echo $host;?>/library/plugins/datatable/jquery.dataTables.js"></script>
        <script src="<?php echo $host;?>/library/plugins/datatable/dataTables.semanticui.js"></script>

        <script>
            $(document).ready(function(){
                $(".dropdown").dropdown();
            });

            current_url = "<?php echo $current_url_no_variables; ?>";

            switch(current_url){
                case '/index.php':
					$("#left_sidebar_home").addClass("active");
				break;

                case '/':
					$("#left_sidebar_home").addClass("active");
                break;
                
                case '/pages/list_categories.php':
                    $("#left_sidebar_categories").addClass("active");
                break;

                case '/pages/import_products.php':
                    $("#left_sidebar_import_products").addClass("active");
                break;
            }


            function wait_on(){ 
                HoldOn.open({
                    theme:"sk-circle",
                    message:"Carregando..."
                });
            }

            function wait_off(){
                HoldOn.close();
            }

            function remove_modal(idmodal){
                $("#"+idmodal).remove();
            }

            function close_modal(idmodal){
                $("#"+idmodal).modal("toggle");
            }

            function sweetalert_modal(type, message, button_text){
                swal({
                    html:message,
                    type: type,
                    confirmButtonText: button_text
                });
            };

            function currency2db(string_value,lang){
                switch(lang){
                    default:
                        string_value = string_value.replace(".", "");
                        string_value = string_value.replace(",", ".");
                    return string_value;
                }
            }

        </script>
    </body>
</html>