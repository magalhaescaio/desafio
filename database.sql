create database if not exists webjump;

drop table if exists webjump.products;

create table if not exists webjump.products(
   id_product int primary key auto_increment,
   product_name varchar(255) not null,
   sku varchar(255) null,
   price decimal(10, 2) null,
   quantity int null,
   category varchar(255) null,
   description mediumtext null,
   image varchar(510) null,
   created  datetime default now(),
   owner  varchar(255) null,
   modified datetime null,
   responsible_modification  varchar(255) null,
   active bit default 1
);

create table if not exists webjump.categories(
   id_category int primary key auto_increment,
   category_name varchar(255) null,
   created datetime default now(),
   owner  varchar(255) null,
   modified datetime null,
   responsible_modification  varchar(255) null,
   active bit default 1
);