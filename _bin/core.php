<?php
    error_reporting(0);

    
    //caminho principal
	$host = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER['HTTP_HOST'];  
    
    $current_url = $_SERVER['REQUEST_URI'];
    $c = explode("?", $current_url);
    $current_url_no_variables = $c[0];

    $file_favicon = "";
    $file_logo = $host."/library/images/main.png";


    $user_name = "Caio Magalhães";
    $user_profile_picture = $host."/library/images/avatar/Abraham.png";

    
    //configuração à ser utilizada no banco de dados
	$site_config = 'localhost'; //production, localhost, etc 
    
    //Parâmetros do banco
    switch($site_config){
        case 'localhost':

        $db_host = "localhost";
        $db_name = "webjump";
        $db_user = "root";
        $db_password  = "";
        $db_port = "3306";

        break;
    }
    
    //Classe para conexão no banco de dados
    class database{
        private $connection;

        function __construct(){
            GLOBAL $db_host;
            GLOBAL $db_user;
            GLOBAL $db_name;
            GLOBAL $db_password;
            GLOBAL $db_port;
            
			$this->connection= mysqli_connect('p:'.$db_host, $db_user, $db_password, $db_name, $db_port)  or die ("Connection failed: " . mysqli_error($connessione));

        }
        
        function select($query){
            $result = mysqli_query($this->connection, $query) or die("Query Failed '$query': " . mysqli_error($this->connection) );
            error_reporting(0);

		    $xml = "";
	        
    		$this->num_rows = mysqli_num_rows($result);  
		    
            $field_num = 1;
            
            while ($fieldinfo=mysqli_fetch_field($result)){
                $campi[$field_num] = $fieldinfo->name;
                $type[$field_num] = $fieldinfo->type;
                $field_num = $field_num+1;
            }
		
            $i_array=0;
        
            while ($linea = mysqli_fetch_array($result, MYSQLI_ASSOC)){                
                for($i=1;$i<$field_num;$i++){
                    
                    $value = $linea[$campi[$i]];
                    $this->row[$i_array]->{$campi[$i]} = $value;
                    $this->$campi[$i] = $value;
                }

                $i_array = $i_array +1;
            }
        }

        function insert($query){
            $result = mysqli_query($this->connection, $query) or die("Query Failed '$query': " . mysqli_error($this->connection) );

            $this->id = mysqli_insert_id($this->connection);  
        }

        function update($query){
            $result = mysqli_query($this->connection, $query) or die("Query Failed '$query': " . mysqli_error($this->connection) );
        }

    }


    function randString($size){
        $basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $return= "";

        for($count= 0; $size > $count; $count++){
            $return.= $basic[rand(0, strlen($basic) - 1)];
        }

        return $return;
    }

    function format_datetime($datetime){
        if($datetime){
            $a = explode(" ", $datetime);
    
            $date = $a[0];
            $time = $a[1];
        
            $date = explode("-", $date);
            $new_date = $date[2]."/".$date[1]."/".$date[0];
        
            $time = explode(":", $time);
            $new_time = $time[0].":".$time[1];
        
            $new_datetime = $new_date." ".$new_time;

            return $new_datetime;
        }
    }
?>