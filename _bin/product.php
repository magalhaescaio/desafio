<?php
    include('core.php');
    
    $product_name = $_POST["product_name"];
    $sku = $_POST["sku"];
    $price = $_POST["price"];
    $quantity = $_POST["quantity"];
    $category = $_POST["category"];
    $id_product = $_POST["id_product"];
    $description = $_POST["description"];

    $preset = $_POST["preset"];

    if($category){
        $category = implode("|", $category);
    }
   
    if($_FILES["image"]){
        $path = 'uploads/'; // directory

        $img = $_FILES['image']['name'];

        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

        if($ext){
            $new_file_name = randString(40).".".$ext;
        }
       
        move_uploaded_file($_FILES["image"]["tmp_name"],"uploads/".$new_file_name);
    }   

    class product {
        
        public function add_product($product_name, $sku, $price, $quantity, $category, $new_file_name, $description){
            GLOBAL $user_name;

            $conn = new database;

            $conn->insert("INSERT INTO products (product_name, sku, price, quantity, category, image, owner, description) VALUES ('$product_name', '$sku', '$price', '$quantity', '$category', '$new_file_name', '$user_name', '$description') ");
        }   

        public function edit_product($product_name, $sku, $price, $quantity, $category, $new_file_name, $id_product, $description){
            GLOBAL $user_name;
            
            $conn = new database;

            $sql = "UPDATE products SET 
                product_name='$product_name'
                , sku='$sku'
                , price='$price'
                , quantity='$quantity'
                , category='$category'
                , modified=now()
                , responsible_modification='$user_name'
                , description='$description' ";

                if($new_file_name){
                    $sql .= ", image='$new_file_name' ";
                }

                $sql .= "WHERE id_product='$id_product' ";

            $conn->update($sql);
        }

        public function delete_product($id_product){
            GLOBAL $user_name;

            $conn = new database;
            $conn->update("UPDATE products SET active=0, modified=now(), responsible_modification='$user_name' WHERE id_product='$id_product' ");
        }

        public function list_active_products(){
            $conn = new database;
            $conn->select("SELECT products.*  FROM products  WHERE products.active = 1 ");
            
            return $conn;
        }

        public function get_product_info($id_product){
            $conn = new database;
            $conn->select("SELECT * FROM products WHERE id_product=$id_product ");
            
            return $conn;
        }

        public function search_products($product_name, $sku, $category){
            $conn = new database;

            $sql = "SELECT products.* FROM products  WHERE products.active = 1 ";

            if($product_name){
                $sql .= " AND products.product_name LIKE '%$product_name%' ";
            } 
            
            if($sku){
                $sql .= " AND products.sku LIKE '%$sku%' ";
            } 

            if($category){
                $sql .= " AND products.category LIKE '%$category%' ";
            } 

            $conn->select($sql);

            return $conn;
        }

        
    }


    switch($preset){
        case 'add_new_product':
            $add_product = new product;
            $add_product->add_product($product_name, $sku, $price, $quantity, $category, $new_file_name, $description);
        break;
        
        case 'edit_product':
            $edit_product = new product;
            $edit_product->edit_product($product_name, $sku, $price, $quantity, $category, $new_file_name, $id_product, $description);
        break;

        case 'delete_product':
            $delete_product = new product;

            $delete_product->delete_product($id_product);

        break;
    }

    


  

?>