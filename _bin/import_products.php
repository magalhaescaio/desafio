<?php
include("product.php");

$random_string = $_POST["random_string"];
$filename = $_POST["filename"];


$ext = pathinfo($filename, PATHINFO_EXTENSION);

if($ext != "csv"){
    exit;
}


$filecontent = file("imports/$filename"); 
$total_rows = count($filecontent);

$progress_value = 1;

unset($filecontent[0]);

$ar_insert = array();

foreach($filecontent as $row){

    $row = explode(";", $row);

    $nome = $row[0];
    $sku = $row[1];
    $descricao = $row[2];
    $quantidade = $row[3];
    $preco = $row[4];
    $categoria = $row[5];

    $ar_categoria = explode("|", $categoria);

    foreach($ar_categoria as $cat){
        $cat = trim($cat);
        if($cat != ''|| $cat != null){
            $conn  = new database;
            $conn->select("SELECT * FROM categories WHERE category_name = '$cat' ");

            if($conn->num_rows == 0){
                $conn->insert("INSERT INTO categories (category_name, owner) VALUES ('$cat', '$user_name') ");
            }
        }
    }

    
    $ar_insert[] = "('$nome', '$sku', '$preco', '$quantidade', '$categoria', '$descricao', '$user_name')";

    setProgress(($progress_value * 100) / $total_rows, $random_string);
    $progress_value++;
}

$array_to_insert = implode(",", $ar_insert);

$insert_product = new database;

$query = "INSERT INTO products (product_name, sku, price, quantity, category, description, owner) VALUES  ".$array_to_insert;

$insert_product->update($query);


function setProgress($progress, $file) {
	$file = 'temp/' . $file;
	$progress = round($progress);
	if (!is_file($file))
		touch($file);
	file_put_contents($file, $progress);
}
?>