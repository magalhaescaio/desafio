<?php
    include('core.php');

    $preset = $_POST["preset"];
    $category_name = $_POST["category_name"];
    $id_category = $_POST["id_category"];


    class category{

        public function add_category($category_name){
            GLOBAL $user_name;

            $conn = new database;
            $conn->insert("INSERT INTO categories (category_name, owner) VALUES ('$category_name', '$user_name') ");
        }

        public function edit_category($category_name, $id_category){
            GLOBAL $user_name;

            $conn = new database;
            $conn->update("UPDATE categories SET category_name='$category_name', modified=now(), responsible_modification='$user_name' WHERE id_category='$id_category' ");
        }

        public function delete_category($id_category){
            GLOBAL $user_name;
            
            $conn = new database;
            $conn->update("UPDATE categories SET active=0, modified=now(), responsible_modification='$user_name' WHERE id_category='$id_category' ");
        }

        public function list_active_categories(){
            $conn = new database;
            $conn->select("SELECT * FROM categories WHERE active = 1 ");
            
            return $conn;
        }

        public function get_category_info($id_category){
            $conn = new database;
            $conn->select("SELECT * FROM categories WHERE id_category=$id_category ");
            
            return $conn;
        }

    }

    switch($preset){
        case 'add_new_category':
            $add_category = new category;
            $add_category->add_category($category_name);
        break;

        case 'edit_category':
            $edit_category = new category;
            $edit_category->edit_category($category_name, $id_category);
        break;

        case 'delete_category':
            $delete_category = new category;
            $delete_category->delete_category($id_category);
        break;

    }