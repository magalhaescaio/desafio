<?php
error_reporting(0);

ini_set('max_execution_time', 0);

set_time_limit(0);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

$random_string = $_POST["random_string"];

if (is_file('temp/'.$random_string)) {
    echo file_get_contents('temp/'.$random_string);
} else {
    echo 0;
}

?>