<?php
    include('../header.php');
?>

    <div class="ui breadcrumb" style="margin-left: 20px;">
        <a href="<?php echo $host;?>" class="section" onclick="wait_on();"> Início</a>
        <div class="divider"> / </div>
        <div class="section"> Produtos</div>
    </div>
    
    <div class="ui segment">

        <div class="ui grid" style="margin-bottom: 1px;">
            <div class="ui sixteen wide column">
                <h3><i class="cube icon"></i> Produtos</h3>
            </div>
        </div>

        <div class="ui divider" style="margin:0px !important; padding: 0px !important; margin-bottom: 15px !important;"></div>


        <div class="ui grid">
            <div class="ui sixteen wide column">
                <div class="right floated">
                    <div class="ui button violet icon right labeled tiny" onclick="load_products();">
                        <i class="refresh icon"></i>
                        Recarregar produtos
                    </div>

                    <div class="ui  button blue icon right labeled tiny" onclick="show_modal_add_product();">
                        <i class="plus icon"></i>
                        Adicionar novo produto
                    </div>
                </div>
            </div>
        </div>

        <div id="content-data"></div>


    </div>

<?php
    include('../footer.php');
?>

<script>

    $(document).ready(function(){
        load_products();
    });

    function show_modal_add_product(){
        $.ajax({
            url: "view/modal_add_product.php",
            method: "POST",
            
            beforeSend: function(data){
                wait_on();

                $("#modal_add_product").remove();
                $("body").append("<div class='ui modal' id='modal_add_product' ></div>");
            },

            success: function(data){
                $("#modal_add_product").html(data);

                $('#modal_add_product').modal({
                    centered: false, inverted: false, closable: false, 
                    onHidden: function(){
                        remove_modal('modal_add_product');
                    }
                }).modal('show');

                wait_off();
            }
        });
    }

    function load_products(){
        $.ajax({
            url: "view/list_products.php",
            method: "POST",
            
            beforeSend: function(data){
                $("#content-data").html("<div style='margin-top: 30px;'><center><div class='ui active inline loader' style='margin-bottom: 5px;'></div><br> <b>Carregando...</b></center></div>");
            },

            success: function(data){

                $("#content-data").html(data);
                
            }
        });
    }

</script>