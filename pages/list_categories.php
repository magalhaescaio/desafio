<?php
    include('../header.php');
?>

<div class="ui breadcrumb" style="margin-left: 20px;">
    <a href="<?php echo $host;?>" class="section" onclick="wait_on();"> Início</a>
    <div class="divider"> / </div>
    <div class="section"> Categorias</div>
</div>

<div class="ui segment">

    <div class="ui grid" style="margin-bottom: 1px;">
        <div class="ui sixteen wide column">
            <h3><i class="copy icon"></i> Categorias</h3>
        </div>
    </div>

    <div class="ui divider" style="margin:0px !important; padding: 0px !important; margin-bottom: 15px !important;"></div>

    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="right floated">
                <div class="ui button violet icon right labeled tiny" onclick="load_categories();">
                    <i class="refresh icon"></i>
                    Recarregar categorias
                </div>

                <div class="ui  button blue icon right labeled tiny" onclick="show_modal_add_category();">
                    <i class="plus icon"></i>
                    Adicionar nova categoria
                </div>
            </div>
        </div>
    </div>

    <div id="data-content"></div>

</div>

<?php
    include('../footer.php');
?>

<script>
    $(document).ready(function(){
        load_categories();
    });


    function load_categories(){
        $.ajax({
            url: "view/list_categories.php",
            method: "POST",
            
            beforeSend: function(data){
                $("#data-content").html("<div style='margin-top: 30px;'><center><div class='ui active inline loader' style='margin-bottom: 5px;'></div><br> <b>Carregando...</b></center></div>");
            },

            success: function(data){
                $("#data-content").html(data);
            }
        });
    }

    function show_modal_add_category(){
        $.ajax({
            url: "view/modal_add_category.php",
            method: "POST",
            
            beforeSend: function(data){
                wait_on();

                $("#modal_add_category").remove();
                $("body").append("<div class='ui tiny modal' id='modal_add_category' ></div>");
            },

            success: function(data){
                $("#modal_add_category").html(data);

                $('#modal_add_category').modal({
                    centered: false, inverted: false, closable: false, 
                    onHidden: function(){
                        remove_modal('modal_add_category');
                    }
                }).modal('show');

                wait_off();
            }
        });
    }
</script>