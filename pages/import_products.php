<?php
    include('../header.php');
?>

<div class="ui breadcrumb" style="margin-left: 20px;">
    <a href="<?php echo $host;?>" class="section" onclick="wait_on();"> Início</a>
    <div class="divider"> / </div>
    <div class="section"> Importar Produtos</div>
</div>

<div class="ui segment">

    <div class="ui grid" style="margin-bottom: 1px;">
        <div class="ui sixteen wide column">
            <h3><i class="download icon"></i> Importar Produtos</h3>
        </div>
    </div>

    <div class="ui divider" style="margin:0px !important; padding: 0px !important; margin-bottom: 15px !important;"></div>

    <div id="data-content">
        <form class="ui form" id="form_import_product" onsubmit="return false;"  method="POST" enctype="multipart/form-data">
            <div class="field">
                <label>Arquivo CSV</label>
                <input id="file" type="file" accept=".csv" name="file" required/> 
            </div>

            <div class="ui grid">
                <div class="sixteen wide column">
                    <center>
                        <div class="ui button teal icon right labeled" id="btn_send_file" style="width: 20%" onclick="send_file();">
                            <i class="download icon"></i>
                            Enviar arquivo
                        </div>
                    </center>
                </div>
            <div>
        </form>
    </div>

</div>

<?php
    include('../footer.php');
?>


<script>
    function send_file(){
        $('#form_import_product').form('validate form');

        if($('#form_import_product').form('is valid')){
            $( "#form_import_product" ).submit();
        }
    }

    $(function(){
        form_import_product= $('#form_import_product').form({
            inline : true,
            fields: {
                file: {
                    identifier: 'file',
                    rules: [{
                        type   : 'empty',
                        prompt : 'Selecione um arquivo para continuar'
                    }]
                },
            }
        });
    });

        
    $(document).ready(function(e) {
        $("#form_import_product").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo $host;?>/_bin/import_file.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function() {
                    $("#btn_send_file").addClass("disabled loading");
                },
                success: function(data) {
                   
                   filename  = data;
                   second_step(data);
                },
            
            });
        }));

    });


    function second_step(filename){
        $.ajax({
            url: "<?php echo $host;?>/pages/view/import_product_second_step.php",
            method: "POST",
            data:{
                filename: filename,
            },

            beforeSend: function(data){
                $("#data-content").html("<div style='margin-top: 30px;'><center><div class='ui active inline loader' style='margin-bottom: 5px;'></div><br> <b>Carregando...</b></center></div>");
            },

            success: function(data){
                $("#data-content").html(data);
            }
        });    
    }
</script>