<?php
    include('../_bin/product.php');

    $product_name = $_POST["product_name"];
    $sku = $_POST["sku"];
    $category = $_POST["category"];

    $products = new product;
    $data = $products->search_products($product_name, $sku, $category); 

    if($data->num_rows == 0){
        ?>
        <div class="ui warning icon message">
            <i class="info circle icon"></i>

            <div class="content" style="font-size: 12pt;">
                Nenhum produto localizado, acesso o menu "Produtos" para cadastrar novos produtos
            </div>
        </div>
        <?php
        exit;
    }
?>

<div class="ui stackable centered  grid" style="margin-top: 20px;">
    <?php
        foreach($data->row as $result){
            if($result->image){
                $image = $host."/_bin/uploads/".$result->image;
            }else{
                $image = $host."/library/images/unknow_product.png";
            }
            ?>
            <div class="four wide large screen three wide computer column">
                <div class="ui card" style="width: 100%; ">

                <div class="ui placeholder image" style="background: #fff !important;">
                    <center>
                        <span style="padding: 5px;">
                            <img src="<?php echo $image; ?>" style="width: 100%; height: 200px;" />
                        </span>
                    </center>
                </div>

                <div class="content" style="min-height: 160px;">
                    <center>
                        <a class="header" style="font-weight: bold; font-size: 13pt;">
                            <?php echo $result->product_name; ?>
                        </a>

                        <p style="margin-top: 10px; color: #35BE55; font-weight: bold;">R$ <?php echo $result->price;?></p>

                        <p style="margin-top: 10px; color: #ccc; "> <?php echo $result->quantity;?> disponíveis</p>
                    </center>
                </div>

                </div>
            </div>
            <?php
        }
    ?>
</div>