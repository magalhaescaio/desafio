<?php
    include('../../_bin/product.php');

    $id_product = $_POST['id_product'];


    $product = new product;
    $data = $product->get_product_info($id_product);   
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="warning circle icon yellow"></i>

            <span >Excluir</span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_delete_product');"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    
    <center>
        <b>Tem certeza que deseja excluir a produto "<span style="color: red"><?php echo $data->row[0]->product_name; ?></span>" ?</b>
    </center>
    
</div>

<div class="actions">
    <div class="ui button red icon right labeled" id="cancel_delete_product" onclick="close_modal('modal_delete_product');">
        <i class="remove circle icon"></i>
        Não
    </div>

    <div class="ui button green icon right labeled" id="submit_delete_product" onclick="delete_product(<?php echo $data->row[0]->id_product; ?>);">
        <i class="check circle icon"></i>
        Sim
    </div>
</div>

<script>

function delete_product(id_product){

    $.ajax({
            url: "<?php echo $host; ?>/_bin/product.php",
            method: "POST",
            data:{
                id_product: id_product,
                preset: "delete_product"
            },

            beforeSend: function(data){
                $("#cancel_delete_product").addClass("disabled");
                $("#submit_delete_product").addClass("loading disabled");
            },

            success: function(data){
                close_modal('modal_delete_product');
                sweetalert_modal('success', 'Produto excluído com sucesso', 'Fechar');
                load_products();
            }
        });

}

</script>