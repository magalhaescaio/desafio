<?php
    include('../../_bin/core.php');
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="plus circle icon blue"></i>

            <span>Adicionar novo produto</span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_add_product');"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <form class="ui form" id="form_new_product" onsubmit="return false;"  method="POST" enctype="multipart/form-data">
        <div class="field">
            <label>Nome do produto <span style="color: red; font-size: 12pt;">*</span> </label>

            <input type="text" name="product_name" id="product_name">
        </div>

        <div class="two fields">
            <div class="field">
                <label>SKU (Código) </label>

                <input type="text" name="sku" id="sku">
            </div>

            <div class="field">
                <label>Preço <span style="color: red; font-size: 12pt;">*</span></label>

                <div class="ui labeled input ">
                    <div class="ui label">
                        R$
                    </div>

                    <input type="text" name="price" id="price">
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label>Quantidade</label>

                <div class="ui input fluid field">
                    <input type="number" name="quantity" id="quantity" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" name="itemConsumption" />
                </div>
            </div>

            <div class="field">
                <label>Categoria <span style="color: red; font-size: 12pt;">*</span></label>

                <select class="ui dropdown search category multiple" id="category" name="category[]" multiple>
                    <option value="">Escolher...</option>

                    <?php
                        $conn = new database;
                        $conn->select("SELECT * FROM categories WHERE active=1 ");

                        foreach($conn->row as $result){
                            ?>
                            <option value="<?php echo $result->category_name; ?>"><?php echo $result->category_name; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="field">
            <label>Imagem</label>
            <input id="uploadImage" type="file" accept="image/*" name="image" /> 
        </div>

        <div class="field">
            <label>Descrição</label>
            <textarea name="description" id="description"></textarea>
        </div>

        <input type="hidden" name="preset" value="add_new_product" />
    </form>
</div>

<div class="actions">
    <div class="ui button tiny red icon right labeled" id="cancel_add_product" onclick="close_modal('modal_add_product');">
        <i class="remove circle icon"></i>

        Fechar
    </div>

    <div class="ui button tiny green icon right labeled" id="check_add_product" onclick="save_product();">
        <i class="check circle icon"></i>

        Salvar Produto
    </div>
</div>


<script>

$(document).ready(function(e) {
    $(".dropdown").dropdown();

    $('#price').mask('000.000.000,00', {reverse: true});

    $("#form_new_product").on('submit', (function(e) {
        e.preventDefault();
        $.ajax({
            url: "<?php echo $host;?>/_bin/product.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,

            beforeSend: function() {
                $("#form_new_product :input").prop("disabled", true);
                $(".category").addClass("disabled");
                
                $("#check_add_product").addClass("loading disabled"); //Desativa botões
                $("#cancel_add_product").addClass("disabled");
            },
            success: function(data) {
                console.log(data);
                close_modal('modal_add_product');
                sweetalert_modal('success', 'Produto cadastrado com sucesso', 'Fechar');
                load_products();
            },
           
        });
    }));

});

//Validar formulário
$(function(){
    form_new_product= $('#form_new_product').form({
        inline : true,
        fields: {
            product_name: {
                identifier: 'product_name',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo produto é obrigatório'
                }]
            },
            
            price: {
                identifier: 'price',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo preço é obrigatório'
                }]
            },
            
            category: {
                identifier: 'category',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo categoria é obrigatório'
                }]
            },
        }
    });
});
    
function save_product(){
    $('#form_new_product').form('validate form');

    if($('#form_new_product').form('is valid')){
        $( "#form_new_product" ).submit();
    }
}

</script>