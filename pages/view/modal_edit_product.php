<?php
    include('../../_bin/product.php');

    $id_product = $_POST['id_product'];


    $product = new product;
    $data = $product->get_product_info($id_product);   
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="plus circle icon blue"></i>

            <span>Editar produto nº <?php echo $data->row[0]->id_product; ?></span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_edit_product');"></i>
                </span>
            </div>
        </div>
    </div>
</div>


<div class="scrolling content">
    <form class="ui form" id="form_edit_product" onsubmit="return false;"  method="POST" enctype="multipart/form-data">
        <div class="field">
            <label>Nome do produto <span style="color: red; font-size: 12pt;">*</span> </label>

            <input type="text" name="product_name" id="product_name" value="<?php echo $data->row[0]->product_name; ?>">
        </div>

        <div class="two fields">
            <div class="field">
                <label>SKU (Código) </label>

                <input type="text" name="sku" id="sku" value="<?php echo $data->row[0]->sku; ?>">
            </div>

            <div class="field">
                <label>Preço <span style="color: red; font-size: 12pt;">*</span></label>

                <div class="ui labeled input ">
                    <div class="ui label">
                        R$
                    </div>

                    <input type="text" name="price" id="price" value="<?php echo $data->row[0]->price; ?>">
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label>Quantidade</label>

                <div class="ui input fluid field">
                    <input type="number" value="<?php echo $data->row[0]->quantity; ?>" name="quantity" id="quantity" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" name="itemConsumption" />
                </div>
            </div>

            <div class="field">
                <label>Categoria <span style="color: red; font-size: 12pt;">*</span></label>

                <?php
                    $ar_category = $data->row[0]->category;

                    $ar_category = explode("|", $ar_category);

                    
                ?>
                <select class="ui dropdown search category multiple" id="category" name="category[]" multiple>
                    <option value="">Escolher...</option>

                    <?php
                        $conn = new database;
                        $conn->select("SELECT * FROM categories WHERE active=1 ");

                        foreach($conn->row as $result){
                            if (in_array("$result->category_name", $ar_category)) { 
                                $selected = "selected";
                            }else{
                                $selected = "";
                            }
                           ?>
                             <option value="<?php echo $result->category_name; ?>" <?php echo $selected; ?> > <?php echo $result->category_name; ?></option>
                           <?php
                            
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="ui grid">
            <div class="eight wide column">
                <div class="field">
                    <label>Imagem</label>
                    <input id="uploadImage" type="file" accept="image/*" name="image" /> 
                </div>
            </div>
            <div class="eight wide column">
                <?php
                    if($data->row[0]->image){
                        $image = $host."/_bin/uploads/".$data->row[0]->image;
                    }else{
                        $image = $host."/library/images/unknow_product.png";
                    }
                ?>

                <center><img src="<?php echo $image?>" style="width: 200px; border: 1px solid #ccc;"/></center>
            </div>
        </div>

        <div class="field">
            <label>Descrição</label>
            <textarea name="description" id="description"><?php echo $data->row[0]->description; ?></textarea>
        </div>

        <input type="hidden" name="preset" value="edit_product" />
        <input type="hidden" name="id_product" value="<?php echo $data->row[0]->id_product; ?>" />
    </form>
</div>

<div class="actions">
    <div class="ui button tiny red icon right labeled" id="cancel_edit_product" onclick="close_modal('modal_edit_product');">
        <i class="remove circle icon"></i>

        Fechar
    </div>

    <div class="ui button tiny blue icon right labeled" id="check_edit_product" onclick="edit_product();">
        <i class="check circle icon"></i>

        Salvar alterações
    </div>
</div>

<script>

    $(document).ready(function(){

        $(".dropdown").dropdown();

        $('#price').mask('000.000.000,00', {reverse: true});

        $("#form_edit_product").on('submit', (function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo $host;?>/_bin/product.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function() {
                    $("#form_edit_product :input").prop("disabled", true);
                    $(".category").addClass("disabled");
                    
                    $("#check_edit_product").addClass("loading disabled"); //Desativa botões
                    $("#cancel_edit_product").addClass("disabled");
                },
                success: function(data) {
                    close_modal('modal_edit_product');
                    sweetalert_modal('success', 'Produto editado com sucesso', 'Fechar');
                    load_products();
                },
            
            });
        }));

    });
    
    
    
//Validar formulário
$(function(){
    form_edit_product= $('#form_edit_product').form({
        inline : true,
        fields: {
            product_name: {
                identifier: 'product_name',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo produto é obrigatório'
                }]
            },
            
            price: {
                identifier: 'price',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo preço é obrigatório'
                }]
            },
            
            category: {
                identifier: 'category',
                rules: [{
                    type   : 'empty',
                    prompt : 'O campo categoria é obrigatório'
                }]
            },
        }
    });
});
    
function edit_product(){
    $('#form_edit_product').form('validate form');

    if($('#form_edit_product').form('is valid')){
        $( "#form_edit_product" ).submit();
    }
}

</script>