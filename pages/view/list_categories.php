<?php
    include('../../_bin/category.php');


    $category = new category;
    $data = $category->list_active_categories();  

     if($data->num_rows == 0){
        ?>
        <div class="ui warning message icon" style="margin-top: 30px;">
            <i class="warning circle icon" style="font-size: 15pt;"></i>

            <div class="content">
                Nenhuma categoria localizada
            </div>
        </div>
        <?php
        exit();
    } 
?>
<div class="ui info message icon" style="margin-top: 30px;">
    <i class="info circle icon" style="font-size: 15pt;"></i>

    <div class="content">
        Foram encontrado(s) <?php echo $data->num_rows; ?> resultado(s)
    </div>
</div>

<table class="ui table celled no-footer" style="margin-top: 30px;">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nome Categoria</th>
            <th>Responsável</th>
            <th>Cadastro</th>
            <th>Alteração</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <?php
            foreach($data->row as $result){
                $created = format_datetime($result->created);

                $modified = format_datetime($result->modified);
                
                if($modified){
                    $modified = "<span data-tooltip='Editado por $result->responsible_modification'><i class='user icon'></i> ".$modified."</span>";
                }

                ?>
                <tr>
                    <td><?php echo $result->id_category; ?></td>
                    <td><?php echo $result->category_name; ?></td>
                    <td><?php echo $result->owner; ?></td>
                    <td><?php echo $created; ?></td>
                    <td><?php echo $modified; ?></td>
                    <td>
                        <center>
                            <span data-tooltip="Editar">
                                <i class="edit blue icon pointer" style="font-size: 13pt" onclick="show_modal_edit_category(<?php echo $result->id_category; ?>)"></i>
                            </span>

                            <span data-tooltip="excluir">
                                <i class="trash red icon pointer" style="font-size: 13pt" onclick="show_modal_delete_category(<?php echo $result->id_category; ?>)"></i>
                            </span>
                        </center>
                    </td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>

<script>

function show_modal_edit_category(id_category){
    $.ajax({
        url: "view/modal_edit_category.php",
        method: "POST",

        data:{
            id_category: id_category
        },
        
        beforeSend: function(data){
            wait_on();

            $("#modal_edit_category").remove();
            $("body").append("<div class='ui tiny modal' id='modal_edit_category' ></div>");
        },

        success: function(data){
            $("#modal_edit_category").html(data);

            $('#modal_edit_category').modal({
                centered: false, inverted: false, closable: false, 
                onHidden: function(){
                    remove_modal('modal_edit_category');
                }
            }).modal('show');

            wait_off();
        }
    });
}

function show_modal_delete_category(id_category){
    $.ajax({
        url: "view/modal_delete_category.php",
        method: "POST",

        data:{
            id_category: id_category
        },
        
        beforeSend: function(data){
            wait_on();

            $("#modal_delete_category").remove();
            $("body").append("<div class='ui mini modal' id='modal_delete_category' ></div>");
        },

        success: function(data){
            $("#modal_delete_category").html(data);

            $('#modal_delete_category').modal({
                centered: false, inverted: false, closable: false, 
                onHidden: function(){
                    remove_modal('modal_delete_category');
                }
            }).modal('show');

            wait_off();
        }
    });
}
</script>