<?php
    include('../../_bin/category.php');

    $id_category = $_POST['id_category'];


    $category = new category;
    $data = $category->get_category_info($id_category);   
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="warning circle icon yellow"></i>

            <span >Excluir</span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_delete_category');"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <center>
        <b>Tem certeza que deseja excluir a categoria "<span style="color: red"><?php echo $data->row[0]->category_name; ?></span>" ?</b>
    </center>
</div>

<div class="actions">
    <div class="ui button red icon right labeled" id="cancel_delete_category" onclick="close_modal('modal_delete_category');">
        <i class="remove circle icon"></i>
        Não
    </div>

    <div class="ui button green icon right labeled" id="submit_delete_category" onclick="delete_category(<?php echo $data->row[0]->id_category; ?>);">
        <i class="check circle icon"></i>
        Sim
    </div>
</div>

<script>

    function delete_category(id_category){
        $.ajax({
            url: "<?php echo $host; ?>/_bin/category.php",
            method: "POST",
            data:{
                id_category: id_category,
                preset: "delete_category"
            },

            beforeSend: function(data){
                $("#cancel_delete_category").addClass("disabled");
                $("#submit_delete_category").addClass("loading disabled");
            },

            success: function(data){
                close_modal('modal_delete_category');
                sweetalert_modal('success', 'Categoria foi excluída com sucesso', 'Fechar');
                load_categories();
            }
        });

    }
</script>