<?php
    include('../../_bin/core.php');
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="plus circle icon blue"></i>

            <span >Adicionar nova categoria</span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_add_category');"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <form id="form_new_category" onsubmit="return false;" class="ui form">
        <div class="field">
            <label>Nome da categoria <span style="color: red; font-size: 12pt;">*</span></label>

            <input type="text" name="category_name" id="category_name" >
        </div>
    </form>
</div>

<div class="actions">
    <div class="ui button tiny red icon right labeled" id="cancel_add_category" onclick="close_modal('modal_add_category');">
        <i class="remove circle icon"></i>
        Fechar
    </div>

    <div class="ui button tiny green icon right labeled" id="check_add_category" onclick="save_category();">
        <i class="check circle icon"></i>
        Salvar categoria
    </div>
</div>

<script>

    $(function(){
		form_new_category= $('#form_new_category').form({
			inline : true,
			fields: {
				category_name: {
					identifier: 'category_name',
					rules: [{
						type   : 'empty',
						prompt : 'O campo nome do produto é obrigatório'
					}]
                }
			}
		});
	});
    function save_category(){
        $('#form_new_category').form('validate form');

        if($('#form_new_category').form('is valid')){
            $.ajax({
                url: "<?php echo $host; ?>/_bin/category.php",
                method: "POST",
                data:{
                    preset: "add_new_category",
                    category_name: $("#category_name").val()
                },

                beforeSend: function(data){
                    $("#form_new_category :input").prop("disabled", true);

                    $("#check_add_category").addClass("loading disabled");
                    $("#cancel_add_category").addClass("disabled");
                },

                success: function(data){                    
                    close_modal('modal_add_category');
                    sweetalert_modal('success', 'Categoria foi cadastrada com sucesso', 'Fechar');
                    load_categories();
                }
            });
        }

    }
</script>