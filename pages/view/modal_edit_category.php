<?php
    include('../../_bin/category.php');

    $id_category = $_POST["id_category"];

    $category = new category;
    $data = $category->get_category_info($id_category);   
?>

<div class="header">
    <div class="ui grid">
        <div class="fourteen wide column">
            <i class="edit icon blue"></i>

            <span>Editar categoria nº <?php echo $data->row[0]->id_category; ?></span>
        </div>

        
        <div class="two wide column">
            <div class="right floated">
                <span data-tooltip="Fechar">
                    <i class="remove icon pointer" onclick="close_modal('modal_edit_category');"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <form id="form_edit_category" onsubmit="return false;" class="ui form">
        <div class="field">
            <label>Nome da categoria <span style="color: red; font-size: 12pt;">*</span></label>

            <input type="text" name="category_name" id="category_name" value="<?php echo $data->row[0]->category_name; ?>" >
            
        </div>
    </form>
</div>

<div class="actions">
    <div class="ui button tiny red icon right labeled" id="cancel_edit_category" onclick="close_modal('modal_edit_category');">
        <i class="remove circle icon"></i>
        Fechar
    </div>

    <div class="ui button tiny blue icon right labeled" id="check_edit_category" onclick="edit_category();">
        <i class="edit icon"></i>
        Salvar alterações
    </div>
</div>

<script>

    $(function(){
		form_edit_category= $('#form_edit_category').form({
			inline : true,
			fields: {
				category_name: {
					identifier: 'category_name',
					rules: [{
						type   : 'empty',
						prompt : 'O campo nome do produto é obrigatório'
					}]
                }
			}
		});
	});
    function edit_category(){
        $('#form_edit_category').form('validate form');

        if($('#form_edit_category').form('is valid')){
            $.ajax({
                url: "<?php echo $host; ?>/_bin/category.php",
                method: "POST",
                data:{
                    preset: "edit_category",
                    category_name: $("#category_name").val(),
                    id_category: "<?php echo $data->row[0]->id_category; ?>"
                },

                beforeSend: function(data){
                    $("#form_edit_category :input").prop("disabled", true);

                    $("#check_edit_category").addClass("loading disabled");
                    $("#cancel_edit_category").addClass("disabled");
                },

                success: function(data){                    
                    close_modal('modal_edit_category');
                    sweetalert_modal('success', 'Categoria foi editada com sucesso', 'Fechar');
                    load_categories();
                }
            });
        }

    }
</script>