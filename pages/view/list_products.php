<?php
    include('../../_bin/product.php');


    $products = new product;
    $data = $products->list_active_products();  

    if($data->num_rows == 0){
        ?>
        <div class="ui warning message icon" style="margin-top: 30px;">
            <i class="warning circle icon" style="font-size: 15pt;"></i>

            <div class="content">
                Nenhum produto cadastrado localizado
            </div>
        </div>
        <?php
        exit();
    }
?>

<div class="ui info message icon" style="margin-top: 30px;">
    <i class="info circle icon" style="font-size: 15pt;"></i>

    <div class="content">
        Foram encontrado(s) <?php echo $data->num_rows; ?> resultado(s)
    </div>
</div>

<table class="ui table celled no-footer" style="margin-top: 30px;">
    <thead>
        <tr>
            <th>ID</th>
            <th width="25%">Nome</th>
            <th>Categoria</th>
            <th>SKU</th>
            <th>Cadastro</th>
            <th>Responsável</th>
            <th>Alteração</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <?php
            foreach($data->row as $result){
                $created = format_datetime($result->created);

                $modified = format_datetime($result->modified);

                if($modified){
                    $modified = "<span data-tooltip='Editado por $result->responsible_modification'><i class='user icon'></i> ".$modified."</span>";
                }
                ?>
                <tr>
                    <td><?php echo $result->id_product; ?></td>
                    <td><?php echo $result->product_name; ?></td>

                    <td>
                        <?php 
                            $category = explode("|", $result->category);

                            foreach($category as $a){
                                echo $a."<br>";
                            }
                        ?>
                    </td>
                    
                    <td><?php echo $result->sku; ?></td>
                    <td><?php echo $created; ?></td>
                    <td><?php echo $result->owner; ?></td>
                    <td><?php echo $modified; ?></td>
                    <td>
                        <center>
                            <span data-tooltip="Editar">
                                <i class="edit icon blue pointer" onclick="show_modal_edit_product(<?php echo $result->id_product; ?>);"></i>
                            </span>

                            <span data-tooltip="Excluir">
                                <i class="trash icon red pointer" onclick="show_modal_delete_product(<?php echo $result->id_product; ?>);"></i>
                            </span>
                        </center>
                    </td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>


<script>

function show_modal_edit_product(id_product){
    $.ajax({
        url: "view/modal_edit_product.php",
        method: "POST",

        data:{
            id_product: id_product
        },
        
        beforeSend: function(data){
            wait_on();

            $("#modal_edit_product").remove();
            $("body").append("<div class='ui modal' id='modal_edit_product' ></div>");
        },

        success: function(data){
            $("#modal_edit_product").html(data);

            $('#modal_edit_product').modal({
                centered: false, inverted: false, closable: false, 
                onHidden: function(){
                    remove_modal('modal_edit_product');
                }
            }).modal('show');

            wait_off();
        }
    });
}

function show_modal_delete_product(id_product){
    $.ajax({
        url: "view/modal_delete_product.php",
        method: "POST",

        data:{
            id_product: id_product
        },
        
        beforeSend: function(data){
            wait_on();

            $("#modal_delete_product").remove();
            $("body").append("<div class='ui mini modal' id='modal_delete_product' ></div>");
        },

        success: function(data){
            $("#modal_delete_product").html(data);

            $('#modal_delete_product').modal({
                centered: false, inverted: false, closable: false, 
                onHidden: function(){
                    remove_modal('modal_delete_product');
                }
            }).modal('show');

            wait_off();
        }
    });
}

</script>