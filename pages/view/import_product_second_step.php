<?php
    include("../../_bin/core.php");

    $filename = $_POST["filename"];

?>


<div class="ui large indicating progress active" data-value="1" data-total="100" id="main_bar_progress" data-percent="0" style="margin-top: 30px;">
    <div class="bar" style="transition-duration: 300ms; width: 0%;" id="bar_progress">
        <div class="progress" id="progress_description">0%</div>
    </div>
    <div class="label" id="bar_description" style="font-size: 13pt; margin-top: 20px;">
        <div class="ui active small inline loader"></div> &nbsp; 
        Carregando produtos...
    </div>
</div><br><br><br>


<script>
	random_string = "<?php echo substr(md5(microtime()),1,rand(24,24)); ?>";

	$.ajax({
		method: 'POST',
		url: '<?php echo $host; ?>/_bin/create_temp_file.php',
		data:{
			random_string:random_string
		},
		success: function (data) {
			//Inicia execução do script
			start();
		}
	});

    function start(){
		$(function () {
			request._interval = setInterval(request.checkStatus, 1000);
			$.ajax({
				method: 'POST',
				url: "<?php echo $host;?>/_bin/import_products.php",
				data:{
					random_string:random_string,
					filename: "<?php echo $filename; ?>"
				},
				success: function (data) {
					delete_temp_file(random_string);
					
					$("#progress_description").html("100%");
					$("#bar_progress").css("width", "100%");
					$("#main_bar_progress").attr("data-percent", "100");
					$("#main_bar_progress").addClass("success");
					$("#bar_description").html("<i class='check circle icon'></i> Produtos Importados <br><br> <a href='<?php echo $host; ?>/pages/list_products.php' class='ui button blue'>Listar Produtos</a> <br>");
					$("#content_data").html(data);
					request.clearInterval();
					request.setStatus(100);
				},
				error: function () {
					request.setStatus(0);
					request.clearInterval();
					$("#progress_description").html("100%");
					$("#bar_progress").css("width", "100%");
					$("#main_bar_progress").attr("data-percent", "100");
					$("#main_bar_progress").removeClass("success");
					$("#main_bar_progress").addClass("error");
					$("#bar_description").html("<p class='text-error'><i class='remove circle icon'></i> Erro ao importar produtos</p>");
				}
			});
		});
	}


    var request = {
		checkStatus: function () {
			$.ajax({
				method: 'POST',
				url: '<?php echo $host;?>/_bin/status_temp_file.php',
				data:{
					random_string:random_string
				},
				success: function (data) {
					if(data)
						request.setStatus(data);
				}
			});
		},
		setStatus: function (status) {
			$("#progress_description").html(status+" %");
			$("#bar_progress").css("width", status+"%");
			$("#main_bar_progress").attr("data-percent", status);
		},
		_interval: null,
		clearInterval: function () {
			clearInterval(request._interval);
		}
	};

    function delete_temp_file(filename){
		$.ajax({
			method: 'POST',
			url: '<?php echo $host;?>/_bin/delete_temp_file.php',
			data:{
				filename:filename
			},
			success: function(data){
				$("#progress_description").html("100%");
				$("#bar_progress").css("width", "100%");
				$("#main_bar_progress").attr("data-percent", "100");
				$("#main_bar_progress").addClass("success");
			}
		});
	}
</script>