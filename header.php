<?php

include("_bin/core.php");

?>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>WebJump</title>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="shortcut icon" href="<?php echo $file_favicon;?>">

        <link href="<?php echo $host;?>/library/semantic/semantic.min.css" rel="stylesheet" />
        <link href="<?php echo $host;?>/library/plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
        <link href="<?php echo $host;?>/library/css/main.css" rel="stylesheet">
        <link href="<?php echo $host;?>/library/plugins/quill/quill.snow.css" rel="stylesheet">
        <link href="<?php echo $host;?>/library/plugins/hold_on/src/css/HoldOn.css" rel="stylesheet">
        <link href="<?php echo $host;?>/library/plugins/datatable/dataTables.semanticui.css" rel="stylesheet">


    </head>

    <body>

        <div id="contextWrap"> <!-- contextWrap -->

            <div class="ui sidebar vertical left menu overlay  borderless visible sidemenu inverted" style="-webkit-transition-duration: 0.1s; transition-duration: 0.1s; font-size: 10pt;" data-color="dark"> <!-- sidebar -->
                <div class="item logo pointer" style="border-bottom: 1px solid rgba(34, 36, 38, .15); background-color: white">
                    <center><img class="ui image" src="<?php echo $file_logo;?>" style="max-width: 150px !important; height: 35px !important; "></center>
                </div>

                <a class="item" id="left_sidebar_home" href="<?php echo $host;?>"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="home  titleIcon icon"></i> Início
			    </a>

                <a class="item" id="left_sidebar_products" href="<?php echo $host;?>/pages/list_products.php"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="cube  titleIcon icon"></i> Produtos
			    </a>

                
                <a class="item" id="left_sidebar_import_products" href="<?php echo $host;?>/pages/import_products.php"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="upload titleIcon icon"></i> Importar Produtos
			    </a>

                <a class="item" id="left_sidebar_categories" href="<?php echo $host;?>/pages/list_categories.php"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="copy titleIcon icon"></i> Categorias
			    </a>

                

                <!-- <a class="item" id="left_sidebar_logs" href="<?php echo $host;?>/pages/list_logs.php"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="list titleIcon icon"></i> Log de ações
			    </a>

                <a class="item" id="left_sidebar_settings" href="<?php echo $host;?>/pages/settings.php"style="border-bottom: 1px solid rgba(102,102,102);" onclick="wait_on();">
				    <i class="settings titleIcon icon"></i> Configurações
			    </a> -->

            </div><!-- sidebar -->

            <div class="navslide navwrap"><!-- navslide -->
				<div class="ui top fixed menu icon borderless grid" data-color="inverted white">
					<div class="right menu colhidden">
						<div class="ui dropdown item">
							<img class="ui mini circular image" style="width: 30px !important; height: 30px !important; border: 1px solid #ccc;" src="<?php echo $user_profile_picture; ?>" /> &nbsp;
							<?php echo $user_name; ?>

							<i class="dropdown icon"></i>

							<div class="menu" style="border: 1px solid #7F7F7F;">

								<a class="item" href="" onclick="wait_on();">
									<i class="logout icon"></i> 
									Sair
								</a>

							</div>

						</div>

					</div>
				</div>
			</div><!-- navslide -->

            
            <div style="margin-top: 80px; margin-left: 270px; margin-right: 10px;">
            
           