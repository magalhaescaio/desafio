<?php
    include('header.php');
?>
    <div class="ui breadcrumb" style="margin-left: 20px;">
        <a href="<?php echo $host;?>" class="section" onclick="wait_on();"> Início</a>
    </div>

    <div class="ui segment">

        <form class="ui form">
            <div class="four fields">
                <div class="field">
                    <label>Nome do produto</label>
                    <input type="text" name="product_name" id="product_name"/>
                </div>

                <div class="field">
                    <label>SKU (Código)</label>
                    <input type="text" name="sku" id="sku"/>
                </div>

                <div class="field">
                    <label>Categoria</label>
                    <select class="ui dropdown search category" id="category" name="category">
                        <option value="">Escolher...</option>
                        <?php
                            $conn = new database;
                            $conn->select("SELECT * FROM categories WHERE active=1 ");

                            foreach($conn->row as $result){
                                ?>
                                <option value="<?php echo $result->category_name; ?>"><?php echo $result->category_name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>

                <div class="field">
                    
                    <label>&nbsp;</label>
                    <div class="right floated">
                        <div class="ui button blue icon right labeled mini" onclick="load_index();">
                            <i class="search icon"></i>
                            Pesquisar
                        </div>
                    </div>
                </div>
            </div>
        </form>

       <div id="data-content"></div>
    </div>

<?php
    include('footer.php');
?>

<script>
    $(document).ready(function(){
        load_index();
    });


    function load_index(){
        $.ajax({
            url: "<?php echo $host; ?>/pages/dashboard.php",
            method: "POST",
            data:{
                product_name: $("#product_name").val(),
                sku: $("#sku").val(),
                category: $("#category").val(),
            },

            beforeSend: function(data){
                $("#data-content").html("<div style='margin-top: 30px;'><center><div class='ui active inline loader' style='margin-bottom: 5px;'></div><br> <b>Carregando...</b></center></div>");
            },

            success: function(data){
                $("#data-content").html(data);
            }
        });
    }
</script>